package com.cooleskino;

import static org.junit.Assert.*;


import org.junit.Test;

public class SitzBelegenTest {

	@Test
	public void testBelegen() {
		Sitzkategorie kategorie=new Sitzkategorie(5,"test");
		Sitzplatz sitz = new Sitzplatz(1,1);
		sitz.setKategorie(kategorie);
		SitzBelegtFrei sitzB = new SitzBelegtFrei(sitz);
		
		sitzB.sitzBelegen();
		boolean output = sitzB.isBelegt();
		
		assertTrue(output);
	}
	
	@Test
	public void testDoppeltBelegen() {
		Sitzkategorie kategorie=new Sitzkategorie(5,"Parkett");
		Sitzplatz sitz = new Sitzplatz(1,1);
		SitzBelegtFrei sitzB = new SitzBelegtFrei(sitz);
		
		String output="";
		try {
		sitzB.sitzBelegen();
		sitzB.sitzBelegen();
		}catch(IllegalArgumentException e) {
			output = e.getMessage();
		}
		
		assertEquals("Sitz belegt",output);
	}
	
	@Test
	public void testFreigeben() {
		Sitzkategorie kategorie=new Sitzkategorie(5,"Parkett");
		Sitzplatz sitz = new Sitzplatz(1,1);
		SitzBelegtFrei sitzB = new SitzBelegtFrei(sitz);
		
		sitzB.sitzBelegen();
		sitzB.sitzFreigeben();
		boolean output = sitzB.isBelegt();
		
		assertFalse(output);
	}
	
	@Test
	public void testFreigebenWennFrei() {
		Sitzkategorie kategorie=new Sitzkategorie(5,"Parkett");
		Sitzplatz sitz = new Sitzplatz(1,1);
		SitzBelegtFrei sitzB = new SitzBelegtFrei(sitz);
		
		String output="";
		try {
		sitzB.sitzFreigeben();
		}catch(IllegalArgumentException e) {
			output = e.getMessage();
		}
		
		assertEquals("Sitz war schon frei",output);
	}

}
