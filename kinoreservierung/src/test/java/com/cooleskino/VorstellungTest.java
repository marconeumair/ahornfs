package com.cooleskino;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class VorstellungTest {

	@Test
	public void testSearch() {
		Calendar datum= Calendar.getInstance();
		Sitzkategorie kat = new Sitzkategorie(5,"Parkett");
		Saal saal = new Saal(3,3,false,"test");
		saal.setSitzkategorie(1, 3, kat);
		Film film = new Film("Test",120,"Beschreibung",12,3,4,false,"Test.jpg");
		Film film1 = new Film("Test1",120,"Beschreibung",12,3,4,false,"Test1.jpg");
		Vorstellung vorstellung = new Vorstellung(datum,saal,film);
		Vorstellung vorstellung2 = new Vorstellung(datum,saal,film1);
		
		assertEquals("Test",Vorstellung.search("Test").get(0).getFilm().getName());
	}
	@Test
	public void testSearchFail() {
		Calendar datum= Calendar.getInstance();
		Sitzkategorie kat = new Sitzkategorie(5,"Parkett");
		Saal saal = new Saal(3,3,false,"test");
		saal.setSitzkategorie(1, 3, kat);
		Film film = new Film("Test",120,"Beschreibung",12,3,4,false,"Test.jpg");
		Film film1 = new Film("Test1",120,"Beschreibung",12,3,4,false,"Test1.jpg");
		Vorstellung vorstellung = new Vorstellung(datum,saal,film);
		Vorstellung vorstellung2 = new Vorstellung(datum,saal,film1);
		String output = "";
		
		ArrayList<Vorstellung> v= Vorstellung.search("bananarama");
				
		assertEquals(0,v.size());
	}

}
