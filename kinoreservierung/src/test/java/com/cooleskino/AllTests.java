package com.cooleskino;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ReservierungTest.class, SitzBelegenTest.class, VorstellungTest.class })
public class AllTests {

}
