package com.cooleskino;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;

public class ReservierungTest {

	@Test
	public void platzReservieren() {
		Calendar datum= Calendar.getInstance();
		String email = "test@mail.com";
		Sitzkategorie kat = new Sitzkategorie(5,"Parkett");
		Saal saal = new Saal(3,3,false,"test");
		saal.setSitzkategorie(1, 3, kat);
		Film film = new Film("Test",120,"Beschreibung",12,3,4,false,"Test.jpg");
		Vorstellung vorstellung = new Vorstellung(datum,saal,film);
		
		ArrayList<Sitzplatz> plaetze = new ArrayList<Sitzplatz>() ;
		plaetze.add(saal.getSitze()[1][2]);
		Reservierung.reservieren(vorstellung, plaetze,email);
		
		assertTrue(vorstellung.getVorstellungsPlaetze()[1][2].isBelegt());
		}
	@Test
	public void platzBelegtTest() {
		String output="";
		Calendar datum= Calendar.getInstance();
		String email = "test@mail.com";
		Sitzkategorie kat = new Sitzkategorie(5,"Parkett");
		Saal saal = new Saal(3,3,false,"test");
		saal.setSitzkategorie(1, 3, kat);
		Film film = new Film("Test",120,"Beschreibung",12,3,4,false,"Test.jpg");
		Vorstellung vorstellung = new Vorstellung(datum,saal,film);
		
		ArrayList<Sitzplatz> plaetze = new ArrayList<Sitzplatz>() ;
		ArrayList<Sitzplatz> plaetze2 = new ArrayList<Sitzplatz>() ;
		plaetze.add(saal.getSitze()[1][2]);
		plaetze2.add(saal.getSitze()[1][2]);
		Reservierung.reservieren(vorstellung, plaetze,email);
		try {
			Reservierung.reservieren(vorstellung, plaetze2,email);
		}catch(Exception e){
			 output = e.getMessage();
		}
		
		
		assertEquals("Sitzplatz ist bereits reserviert!", output);
		}
	@Test
	public void invalidEmail() {
		String output = "";
		Calendar datum= Calendar.getInstance();
		String email = "keine.e-Mail";
		Sitzkategorie kat = new Sitzkategorie(5,"Parkett");
		Saal saal = new Saal(3,3,false,"test");
		saal.setSitzkategorie(1, 3, kat);
		Film film = new Film("Test",120,"Beschreibung",12,3,4,false,"Test.jpg");
		Vorstellung vorstellung = new Vorstellung(datum,saal,film);
		
		ArrayList<Sitzplatz> plaetze = new ArrayList<Sitzplatz>() ;
		plaetze.add(saal.getSitze()[1][2]);
		try {
		Reservierung.reservieren(vorstellung, plaetze,email);
		}catch (Exception e) {
			output = e.getMessage();
		}
		assertFalse(vorstellung.getVorstellungsPlaetze()[1][2].isBelegt());
		assertEquals("Ihre E-Mailadresse ist ungültig", output);
		}
	}
	
	


