package com.cooleskino;

import java.awt.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Vorstellung {
	private static int count=0;
	private Calendar datum; //Date enthält Datum und Uhrzeit
	private Saal saal;
	private int freiePlaetze;
	private Film film;
	private static ArrayList<Vorstellung> Vorstellungsliste = new ArrayList<Vorstellung>();
	private SitzBelegtFrei[][] vorstellungsPlaetze;
	private int id;
	
	public Vorstellung(Calendar datum, Saal saal, Film film) {
		this.datum=datum;
		this.saal = saal;
		this.film = film;
		this.freiePlaetze=saal.anzahlSitzplätze();
		this.id=count;
		
		generateVorstellungsPlaetze();

		Vorstellungsliste.add(this);
		count++;
		
	}
	
	public int getId() {
		return id;
	}
	
	public static Vorstellung findVorstellungById(int id) {
		for(Vorstellung v:Vorstellungsliste) {
			if(v.getId() == id) {
				return v;
			}
		}
		return null;
	}

	public Calendar getDatum() {
		return datum;
	}

	public Saal getSaal() {
		return saal;
	}

	public int getFreiePlaetze() {
		return freiePlaetze;
	}

	public Film getFilm() {
		return film;
	}
	
	public SitzBelegtFrei[][] getVorstellungsPlaetze() {
		return vorstellungsPlaetze;
	}

	public static ArrayList<Vorstellung> getVorstellungsliste() {
		return Vorstellungsliste;
	}


	//Diese Methode erzeugt automatisch die SitzBelegtFrei Objekte für eine Vorstellung
	private void generateVorstellungsPlaetze() {
		Sitzplatz[][] sitzeArray = saal.getSitze();
		SitzBelegtFrei[][] temp = new SitzBelegtFrei[saal.getReihenanzahl()][saal.getSitzeProReihe()];
		for (int i=0;i<sitzeArray.length;i++) {
			for(int j=0;j<sitzeArray[i].length;j++) {
				temp[i][j]= new SitzBelegtFrei(sitzeArray[i][j]);
			}
		}
		this.vorstellungsPlaetze=temp;
	}
	
	//berechnen der aktuell freien plätze anhand von den SitzBelegtFrei Objekten
	public void refreshFreiePlätze() {
		int frei=0;
		for (int i=0;i<vorstellungsPlaetze.length;i++) {
			for(int j=0;j<vorstellungsPlaetze[i].length;j++) {
				if(vorstellungsPlaetze[i][j].isBelegt() == false) {
					frei++;
				}
			}
		}
		this.freiePlaetze=frei;
	}
	// Sucht den namen eines Filmes und gibt alle möglichen Vorstellungen dazu aus, jedoch case sensitiv!
	public static ArrayList<Vorstellung> search (String suchBegriff) {
		ArrayList<Vorstellung> tempList	= new ArrayList<Vorstellung>();	
		for (Vorstellung v : Vorstellungsliste) {
			
			if (v.getFilm().getName().contains(suchBegriff)) {
				tempList.add(v);
			}else {
				//tempList=null;
				//throw new IllegalArgumentException("Vorstellung konnte nicht gefunden werden");
			}
			
		}
		return tempList;
	}
}
	
	
	

