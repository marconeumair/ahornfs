package com.cooleskino;

//Diese Klasse dient dazu einen Sitz für eine bestimmte Vorstellung belegen zu können
public class SitzBelegtFrei {
	private boolean belegt;
	private Sitzplatz platz;
	
	public SitzBelegtFrei(Sitzplatz platz) {
		super();
		this.belegt = false;
		this.platz = platz;
	}
	
	public boolean isBelegt() {
		return belegt;
	}

	public Sitzplatz getPlatz() {
		return platz;
	}

	//Diese Methode belegt einen bestimmten Platz für eine Vorstellung
	public void sitzBelegen() {
		if(belegt==false) {
			belegt=true;
		}else {
			throw new IllegalArgumentException("Sitz belegt");
		}
	}
	
	//Dies Methode gibt einen belegten Platz wieder frei
	public void sitzFreigeben() {
		if(belegt==true) {
			belegt=false;
		}else {
			throw new IllegalArgumentException("Sitz war schon frei");
		}
	}
}
