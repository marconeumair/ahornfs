package com.cooleskino;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Reservierung {
	private List<Sitzplatz> plaetze;
	private double preis;
	private Vorstellung vorstellung;
	private String eMail;
	private static int id = 0;

	private Reservierung(Vorstellung vorstellung, ArrayList<Sitzplatz> sitze, String email) {
		this.vorstellung = vorstellung;
		this.plaetze = sitze;
		setEMail(email);
		preisBerechnen();
		sendBestaetigung();
		id++;
	}

	public static Reservierung reservieren(Vorstellung vorstellung, List<Sitzplatz> plaetze, String email) {
		ArrayList<Sitzplatz> bestPlaetze = new ArrayList<Sitzplatz>();
		vorstellung.refreshFreiePlätze();
		if (isValidEmailAddress(email)== true) {
			for (int i = 0; i < vorstellung.getVorstellungsPlaetze().length; i++) {
				for (int j = 0; j < vorstellung.getVorstellungsPlaetze()[i].length; j++) {
					for (Sitzplatz s : plaetze) {
						if (vorstellung.getVorstellungsPlaetze()[i][j].getPlatz() == s) {
							if (vorstellung.getVorstellungsPlaetze()[i][j].isBelegt() == false) {
								vorstellung.getVorstellungsPlaetze()[i][j].sitzBelegen();
								bestPlaetze.add(vorstellung.getVorstellungsPlaetze()[i][j].getPlatz());
								// es wird der bestätigte Platz zu der Liste hinzugefügt, die wir noch
								// überprüfen!!
								System.out.println("Sitzplatz in Reihe: "
										+ vorstellung.getVorstellungsPlaetze()[i][j].getPlatz().getReihe()
										+ " und der Nummer: "
										+ vorstellung.getVorstellungsPlaetze()[i][j].getPlatz().getPlatzNr()
										+ " wurde reserviert");
							} else {
								throw new IllegalArgumentException("Sitzplatz ist bereits reserviert!");
							}
						}
					}
				}
			}
		} else {
			throw new IllegalArgumentException("Ihre E-Mailadresse ist ungültig");
		}

		return new Reservierung(vorstellung, bestPlaetze, email);
	}

	public List<Sitzplatz> getSitzplatz() {
		return this.plaetze;
	}

	public double getSitzkategorie() {
		return this.preis;
	}

	public Vorstellung getVorstellung() {
		return this.vorstellung;
	}

	public String getEMail() {
		return this.eMail;
	}

	public void setEMail(String eMail) {
		if (isValidEmailAddress(eMail)) {
			this.eMail = eMail;
		} else {
			throw new IllegalArgumentException("Emailadresse ungültig");
		}
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// Damit das funktioniert müsst ihr die javax.mail.jar aus dem nützliches zeug
	// ordner zum build path hinzufügen
	private static boolean isValidEmailAddress(String email) {
		boolean result = false;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
			result = true;
		} catch (AddressException ex) {
			System.out.println("Invalid Email Address");
		}
		return result;
	}

	private void preisBerechnen() {
		double countPreis = 0;
		for (int i = 0; i < plaetze.size(); i++) {
			countPreis += plaetze.get(i).getKategorie().getPreis();
		}
		this.preis = countPreis;
	}

	@SuppressWarnings("deprecation")
	public boolean sendBestaetigung() {
		String to = this.eMail;
		String from = "cooleskino.info@gmail.com";
		String host = "smtp.gmail.com";
		String user = "cooleskino.info@gmail.com";
		String pass = "IntelNvidia";
		boolean sessionDebug = false;
		// Get system properties
		Properties props = System.getProperties();

		// Setup mail server
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.required", "true");

		java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		// Get the default Session object.
		Session session = Session.getDefaultInstance(props, null);
		session.setDebug(sessionDebug);
		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));

			// Set Subject: header field
			message.setSubject("Bestätigung Ihrer Kino Reservierung");

			// Send the actual HTML message, as big as you like
			message.setContent("<h1>Ihre Reservierung wurde bestaetigt.</h1> <p>Ihre Reservierungsnummer lautet: "
					+ this.id + "<br><br> Infos zur Vorstellung: <br><br>Datum: "
					+ this.vorstellung.getDatum().getTime().toString() + "<br>Film: "
					+ this.vorstellung.getFilm().getName() + "<br>Saal: Saal" + this.vorstellung.getSaal().getSaalNr()
					+ "<br>Sitzplaetze:<br>" + this.sitzplatzToString() + "</p>", "text/html");

			// Send message
			Transport transport = session.getTransport("smtp");
			transport.connect(host, user, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			System.out.println("Sent message successfully....");
			return true;
		} catch (MessagingException mex) {
			mex.printStackTrace();
			System.out.println("Email konnte nicht gesendet werden");
			return false;
		}
	}

	public String sitzplatzToString() {
		String s = "";

		for (Sitzplatz sitz : plaetze) {
			s += "Sitz " + sitz.getPlatzNr() + " Reihe " + sitz.getReihe() + "<br>";
		}
		return s;
	}
}
