package com.cooleskino;

//Diese Klasse repräsentiert einen physischen Sitzplatz eines Kinosaals
public class Sitzplatz {
	private int reihe;
	private int platzNr;
	private Sitzkategorie kategorie=null;
	
	public Sitzplatz(int reihe, int platzNr) {
		super();
		this.reihe = reihe;
		this.platzNr = platzNr;
	}

	public int getReihe() {
		return reihe;
	}

	public int getPlatzNr() {
		return platzNr;
	}

	public Sitzkategorie getKategorie() {
		return kategorie;
	}
	
	public void setKategorie(Sitzkategorie kategorie) {
		this.kategorie=kategorie;
	}
	
	
	
	
}
