package com.cooleskino;

public class Sitzkategorie {
	private double preis;
	private String name;
	
	public Sitzkategorie(double preis, String name) {
		super();
		this.preis = preis;
		this.name = name;
	}

	public double getPreis() {
		return preis;
	}

	public String getName() {
		return name;
	}
	
	
	
}
