package com.cooleskino;

import java.util.ArrayList;

public class Film {
	private String name;
	private int laufzeit; // in min.
	private String beschreibung;
	private int fsk;
	private int bewertung;
	private boolean dreiD;
	private boolean FilmRemoved;
	private String plakat;
	private static ArrayList<Film> filmliste= new ArrayList<Film>();
	
	
	
	public Film(String name, int laufzeit, String beschreibung, int fsk, int imKinoSeit, int bewertung, boolean dreiD, String plakat) {
		super();
		setName(name);
		setLaufzeit(laufzeit);
		setBeschreibung(beschreibung);
		setFsk(fsk);
		setBewertung(bewertung);
		setDreiD(dreiD);
		this.plakat=plakat;
		this.FilmRemoved = false;
		filmliste.add(this);
	}
	
	public String getName() {
		return name;
	}
	
	void setName(String name) {
		this.name = name;
	}
	
	int getLaufzeit() {
		return laufzeit;
	}
	
	void setLaufzeit(int laufzeit) {
		if(laufzeit>0) {
			this.laufzeit = laufzeit;
		}else {
			throw new IllegalArgumentException("Die Laufzeit muss größer 0 sein");
		}
	}
	
	String getBeschreibung() {
		return beschreibung;
	}
	
	void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}
	
	int getFsk() {
		return fsk;
	}
	
	void setFsk(int fsk) {
		if(fsk==0 || fsk==6 || fsk==12 || fsk==16 || fsk==18) {
			this.fsk = fsk;
		}else {
			throw new IllegalArgumentException("Der Wert muss 0, 6, 12, 16 oder 18 sein");
		}
		
	}
	
	int getBewertung() {
		return bewertung;
	}
	
	void setBewertung(int bewertung) {
		if(bewertung >=0 && bewertung <=5) {
			this.bewertung = bewertung;
		}else {
			throw new IllegalArgumentException("Bewertung muss zwischen 0 und 5 liegen");
		}
	}
		
	boolean isDreiD() {
		return dreiD;
	}
	void setDreiD(boolean dreiD) {
		this.dreiD = dreiD;
	}
	
	public boolean  FilmRemoved() {
		return this.FilmRemoved;
	}
	
	
	public static ArrayList<Film> getFilmliste() {
		return filmliste;
	}

	public String getPlakat() {
		return plakat;
	}

	//finalize Methode k�nnte noch implementiert werden?
	public void remove() {
		this.FilmRemoved = true;
	}
	
}
