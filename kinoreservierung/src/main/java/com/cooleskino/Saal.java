package com.cooleskino;

//Diese Klasse repräsentiert einen physischen Kinosaal
public class Saal {
	private int saalNr;
	private static int saalCount = 0;
	private int reihenanzahl;
	private int sitzeProReihe;
	private boolean dreiD;
	private String sound;
	private Sitzplatz[][] sitze;
	
	public Saal(int reihenanzahl, int sitzeProReihe, boolean dreiD, String sound) {
		super();
		if(reihenanzahl>0 && sitzeProReihe>0) {
			this.reihenanzahl = reihenanzahl;
			this.sitzeProReihe = sitzeProReihe;
		}else {
			throw new IllegalArgumentException("Beide Zahlen müssen größer als 0 sein");
		}
		this.dreiD = dreiD;
		this.sound = sound;
		saalCount++;
		this.saalNr=saalCount; // Saal Nummer wird jedes mal selber generiert und bleibt einzigartig
		generateSitzplaetze();
	}
	
	public int getSaalNr() {
		return saalNr;
	}


	public int anzahlSitzplätze() {
		return (this.reihenanzahl*this.sitzeProReihe);
	}

	public int getReihenanzahl() {
		return reihenanzahl;
	}

	public int getSitzeProReihe() {
		return sitzeProReihe;
	}

	public boolean isDreiD() {
		return dreiD;
	}

	public String getSound() {
		return sound;
	}
	
	public Sitzplatz[][] getSitze() {
		return sitze;
	}

	//diese Methode erzeugt Sitzplatzobjekte entsprechend der anzahl von reihen und sitzen pro reihe, ohne Kategorie
	private void generateSitzplaetze() {
		Sitzplatz[][] temp = new Sitzplatz[reihenanzahl][sitzeProReihe];
		for(int r=1;r<=this.reihenanzahl;r++) {
			for(int s=1;s<=this.sitzeProReihe;s++) {
				Sitzplatz sitz= new Sitzplatz(r,s);
				temp[r-1][s-1]=sitz;
			}
		}
		this.sitze=temp;
	}
	
	//diese Methode setzt die Kategorie eines sitzes. es muss die reihe von der die kategorie beginnt und bis zu welcher reihe sie geht übergeben werden
	public void setSitzkategorie(int von, int bis, Sitzkategorie kategorie) {
		if(bis<=this.reihenanzahl && von>0) {
			
			int anzahlReihen = bis-von +1;
			
			for(int i=(von-1);i<(von-1)+anzahlReihen;i++) {
				for(int j=0;j<sitzeProReihe;j++) {
					sitze[i][j].setKategorie(kategorie);
				}
			}
		}else {
			throw new IllegalArgumentException("Falsche eingabe der Reihen");
		}
		
	}
	
	@Override
	public void finalize() {
		saalCount--;
	}
}
