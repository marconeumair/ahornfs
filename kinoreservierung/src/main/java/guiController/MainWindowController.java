package guiController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.cooleskino.Film;
import com.cooleskino.Vorstellung;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MainWindowController {

	private static int count = 0;

	@FXML
	private Button nextbtn;
	@FXML
	private Label movover;
	
	@FXML
	private Label label1;
	
	@FXML
	private Label label2;
	
	@FXML
	private Label label3;
	
	@FXML
	private Label headinglabel;

	@FXML
	private Button backbtn;
	
	private Button nextweekbtn;
	private Button nextthisweekbtn;
	private Button nextsoonbtn;
	private Button nextbackbtn;
	@FXML
	private Group chartgroup;

	@FXML
	private AnchorPane overview;

	@FXML
	private VBox filmlist;

	public Main main;

	public void setMain(Main main) {
		this.main = main;
	}

	
	
	@FXML
	public void generateFilmBlock(ActionEvent e) throws IOException {
		filmlist.getChildren().clear();
		
		movover.setText("Vorstellungsübersicht");
		nextbtn.setVisible(true);
		backbtn.setVisible(true);
		
		
		
		chartgroup.setVisible(false);
		label1.setVisible(false);
		label2.setVisible(false);
		label3.setVisible(false);
		headinglabel.setVisible(false);



		if (e.getSource().equals(nextbtn)) {
			count = count + 2;
		} else if (e.getSource().equals(backbtn)) {
			if (count >= 2) {
				count = count - 2;
			} else {
				count = 0;
			}
		} else {
			count = 0;
		}

		for (int i = count; i <= count + 1; i++) {
			if (Film.getFilmliste().size() > i) {
				Film f = Film.getFilmliste().get(i);
				AnchorPane filmBlock = new AnchorPane();
				filmBlock.prefHeight(194.0);
				filmBlock.prefHeight(733.0);

				// Filmtitel
				Label titel = new Label(f.getName());
				titel.setTextFill(Paint.valueOf("white"));
				titel.setLayoutX(5.0);
				titel.setLayoutY(5.0);
				titel.setFont(Font.font(20.0));
				filmBlock.getChildren().add(titel);

				// graues rechteck
				Rectangle r = new Rectangle();
				r.setArcHeight(5.0);
				r.setArcWidth(5.0);
				r.setFill(Paint.valueOf("white"));
				r.setHeight(144.0);
				r.setWidth(733.0);
				r.setLayoutX(0.0);
				r.setLayoutY(36.0);
				r.setOpacity(0.41);
				r.setSmooth(true);
				r.setStroke(Paint.valueOf("black"));
				r.setStrokeType(StrokeType.INSIDE);
				r.setVisible(true);
				filmBlock.getChildren().add(r);

				// Bild
				ImageView bild = new ImageView();
				bild.setFitHeight(121.7);
				bild.setFitWidth(86.0);
				bild.setLayoutX(14.0);
				bild.setLayoutY(47.0);
				bild.setPickOnBounds(true);
				bild.setPreserveRatio(true);
				Image plakat = new Image(f.getPlakat());
				bild.setImage(plakat);
				filmBlock.getChildren().add(bild);

				// Tagesleiste
				HBox h1 = new HBox();
				h1.setLayoutX(123);
				h1.setLayoutY(47);
				h1.setPrefHeight(26);
				h1.setPrefWidth(488);
				String[] tage = { "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag" };
				for (String s : tage) {
					Label l1 = new Label(s);
					l1.setPrefWidth(70);
					l1.setTextFill(Paint.valueOf("darkred"));
					l1.setAlignment(Pos.CENTER);
					l1.setContentDisplay(ContentDisplay.CENTER);
					l1.getStyleClass().add("timefield");
					h1.getChildren().add(l1);
				}
				filmBlock.getChildren().add(h1);

				// Horizontale Box für vorstellungen
				HBox vorstellungen = new HBox();
				vorstellungen.setLayoutX(131);
				vorstellungen.setLayoutY(77);
				vorstellungen.setPrefHeight(100);
				vorstellungen.setPrefWidth(488);

				ArrayList<Vorstellung> v = Vorstellung.search(f.getName());

				for (int j = 0; j <7; j++) {
					// Vorstellungen für einen Tag
					VBox tagVorstellung = new VBox();
					tagVorstellung.setPrefHeight(88.7);
					tagVorstellung.setPrefWidth(70);
					tagVorstellung.setSpacing(0.0);
					if (v != null) {
						for (Vorstellung v1 : v) {
							
							if (v1.getDatum().get(Calendar.DAY_OF_WEEK) == j+2) {
								Button b = new Button(( new SimpleDateFormat( "HH:mm" ) ).format(v1.getDatum().getTime()));
								b.setId(Integer.toString(v1.getId()));
								// Action Listener für den Button, der die Reservierungsseite öffnet
								b.setOnAction(new EventHandler<ActionEvent>() {
									
									@FXML
									public void handle(ActionEvent event) {
										try {
											Button b1 = (Button)event.getSource();
											FXMLLoader loader = new FXMLLoader();
											loader.setLocation(getClass().getResource("Reservierung.fxml"));
											
											AnchorPane pane = loader.load();
											overview.getChildren().setAll(pane);
											
											ReservationController con = loader.getController();
											con.setVorstellung(b1.getId());
											con.createSaalplan();
										
										} catch (IOException e) {
											e.printStackTrace();
										}
									}
								});
								tagVorstellung.getChildren().add(b);
							}
						}
					}
					vorstellungen.getChildren().add(tagVorstellung);

				}
				filmBlock.getChildren().add(vorstellungen);

				filmlist.getChildren().add(filmBlock);
			}
		}
	}

	@FXML
	public void openAboutUs(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("AboutUs.fxml"));
		overview.getChildren().setAll(pane);
	}

	@FXML
	public void openVorstellungsuebersicht(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
		overview.getChildren().setAll(pane);
	}

	@FXML
	public void openreservierung(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("reservierung.fxml"));
		overview.getChildren().setAll(pane);
	}

	@FXML
	public void opencontact(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("Contakt.fxml"));
		overview.getChildren().setAll(pane);
	}

	@FXML
	public void openimpressum(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("Impressum.fxml"));
		overview.getChildren().setAll(pane);
	}

}