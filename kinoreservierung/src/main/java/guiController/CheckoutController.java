package guiController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import com.cooleskino.Reservierung;
import com.cooleskino.Sitzplatz;
import com.cooleskino.Vorstellung;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;

public class CheckoutController {

	private ArrayList<String> idList = new ArrayList();
	private Vorstellung vorstellung;

	@FXML
	private Label parkettcounter;

	@FXML
	private Label logecounter;

	@FXML
	private Label checkoutsum;

	@FXML
	private Label kat;

	@FXML
	private Label kat1;

	@FXML
	private Label katp;

	@FXML
	private Label katp1;

	@FXML
	private Label infos;

	@FXML
	private TextField email;

	@FXML
	private RadioButton r1;

	private RadioButton selected = r1;
	@FXML
	private RadioButton r2;
	@FXML
	private RadioButton r3;
	@FXML
	private RadioButton r4;

	@FXML
	public AnchorPane checkout;

	public Main main;

	public void setMain(Main main) {
		this.main = main;
	}

	public Label getParkettcounter() {
		return parkettcounter;
	}

	public Label getLogecounter() {
		return logecounter;
	}

	public Label getCheckoutsum() {
		return checkoutsum;
	}

	public Label getKat() {
		return kat;
	}

	public Label getKat1() {
		return kat1;
	}

	public Label getKatp() {
		return katp;
	}

	public Label getKatp1() {
		return katp1;
	}

	public Label getInfos() {
		return infos;
	}

	public void setIdList(ArrayList<String> idList) {
		this.idList = idList;
	}

	public void setVorstellung(Vorstellung vorstellung) {
		this.vorstellung = vorstellung;
	}

	@FXML
	public void openAboutUs(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("AboutUs.fxml"));
		checkout.getChildren().setAll(pane);
	}

	@FXML
	public void openVorstellungsuebersicht(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
		checkout.getChildren().setAll(pane);
	}

	@FXML
	public void opencontact(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("Contakt.fxml"));
		checkout.getChildren().setAll(pane);
	}

	@FXML
	public void openimpressum(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("Impressum.fxml"));
		checkout.getChildren().setAll(pane);
	}

	@FXML
	public void openTU(ActionEvent e) throws IOException {

		if (email.getText().equals("")) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Bitte geben Sie Ihre Email an, um die Reservierung abzuschließen.");

			alert.showAndWait();
		} else if (!r1.isSelected()) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Aktuell unterstützen wir nur das Zahlen Vor Ort. Wir Bitten um Entschuldigung.");

			alert.showAndWait();
		} else {

			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Bitte Bestätigen");
			alert.setHeaderText(null);
			alert.setContentText("Möchten Sie jetzt Reservieren?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				Reservierung.reservieren(vorstellung, getSitzauswahl(), email.getText());
				AnchorPane pane = FXMLLoader.load(getClass().getResource("TU.fxml"));
				checkout.getChildren().setAll(pane);
			} else {
				// ... user chose CANCEL or closed the dialog
			}

		}

	}

	private ArrayList<Sitzplatz> getSitzauswahl() {
		ArrayList<Sitzplatz> temp = new ArrayList<Sitzplatz>();

		for (String s : idList) {
			String[] splitText = s.split(":");

			temp.add(
					vorstellung.getVorstellungsPlaetze()[Integer.parseInt(splitText[0])][Integer.parseInt(splitText[1])]
							.getPlatz());
		}

		return temp;
	}
}