package guiController;

import java.util.ArrayList;
import java.util.Calendar;

import com.cooleskino.Film;
import com.cooleskino.Reservierung;
import com.cooleskino.Saal;
import com.cooleskino.Sitzkategorie;
import com.cooleskino.Sitzplatz;
import com.cooleskino.Vorstellung;

public class StartProgram {

	public static void main(String[] args) {
		initializeProgram();
		Main.main(args);

	}

	public static void initializeProgram() {
		Calendar datum = Calendar.getInstance();
		datum.set(2019, Calendar.JANUARY, 29, 15,0);
		String email = "test@mail.com";
		System.out.println(datum.getTime().toString());
		Calendar datum1 = Calendar.getInstance();
		datum1.set(2019, Calendar.JANUARY, 31, 17, 15);;
		Sitzkategorie kat = new Sitzkategorie(5, "Parkett");
		Sitzkategorie kat1 = new Sitzkategorie(7, "Loge");
		Saal saal = new Saal(8, 15, false, "Saal 1");
		Saal saal1 = new Saal(7, 12, false, "Saal 2");
		saal.setSitzkategorie(1, 5, kat);
		saal.setSitzkategorie(6, 8, kat1);
		saal1.setSitzkategorie(1, 5, kat);
		saal1.setSitzkategorie(6, 7, kat1);
		Film film = new Film("American Sniper", 120, "Beschreibung", 12, 3, 4, false,
				"https://www.cinehits.de/images/poster/00009624_1.jpg");
		Film film1 = new Film("Planet der Affen : Survival", 125, "Beschreibung", 12, 3, 4, false,
				"http://intlportal2.s3.foxfilm.com/intlportal2/dev-temp/de-DE/__5a2e64b7cfc7f.jpg");
		Film film2 = new Film("Aquaman", 100, "Beschreibung", 12, 3, 4, false,
				"http://www.filmposter-archiv.de/filmplakat/2018/aquaman-teaser.jpg");
		Vorstellung v = new Vorstellung(datum, saal, film);
		new Vorstellung(datum, saal, film2);
		new Vorstellung(datum1, saal, film);
		new Vorstellung(datum1, saal1, film1);
		new Vorstellung(datum1, saal, film2);
		

		ArrayList<Sitzplatz> plaetze = new ArrayList<Sitzplatz>();
		plaetze.add(saal.getSitze()[1][2]);
		plaetze.add(saal.getSitze()[1][1]);

		Reservierung.reservieren(v, plaetze, email);
		
	}

}
