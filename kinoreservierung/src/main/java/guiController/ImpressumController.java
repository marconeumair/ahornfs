package guiController;

import java.awt.Color;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ImpressumController {
	
	@FXML
	private AnchorPane impressum;
	
	public Main main;
	
	public void setMain (Main main) {
		this.main = main;
	}
	
	@FXML
	public void openAboutUs(ActionEvent e) throws IOException{
		
			AnchorPane pane = FXMLLoader.load(getClass().getResource("AboutUs.fxml"));
	        impressum.getChildren().setAll(pane);
	}
	@FXML
	public void openVorstellungsuebersicht(ActionEvent e) throws IOException{
		
			AnchorPane pane = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
	        impressum.getChildren().setAll(pane);
	}

	@FXML
	public void opencontact(ActionEvent e) throws IOException{
		
			AnchorPane pane = FXMLLoader.load(getClass().getResource("Contakt.fxml"));
			impressum.getChildren().setAll(pane);
	}
	@FXML
	public void openimpressum(ActionEvent e) throws IOException{
		
			AnchorPane pane = FXMLLoader.load(getClass().getResource("Impressum.fxml"));
			impressum.getChildren().setAll(pane);
	}

}