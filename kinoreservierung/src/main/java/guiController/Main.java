package guiController;
	


import java.io.IOException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class Main extends Application {
	
	static Stage primaryStage;
	
	@Override
	public void start(Stage primaryStage){
		Main.primaryStage = primaryStage;
		mainWindow();
	}
	
	
	public void mainWindow(){
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("MainWindow.fxml"));
				AnchorPane pane = loader.load();
				primaryStage.setMinHeight(450.00);
				primaryStage.setMinWidth(800.00);
				primaryStage.setTitle("CoolesKino");
				primaryStage.setResizable(false);
				primaryStage.getIcons().add(new Image("https://cdn2.iconfinder.com/data/icons/cinema-icons/38/1-512.png"));
				Scene scene1 = new Scene(pane);
				scene1.getStylesheets().addAll(this.getClass().getResource("application.css").toExternalForm());
				primaryStage.setScene(scene1);
				
				primaryStage.show();


			} catch (IOException e) {
				e.printStackTrace();
			}



	}


	public static void main(String[] args) {
		launch(args);
	}

}
