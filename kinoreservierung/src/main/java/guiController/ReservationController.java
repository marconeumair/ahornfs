package guiController;

import java.io.IOException;
import java.util.ArrayList;

import com.cooleskino.Vorstellung;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.stage.Stage;

public class ReservationController {

	private int seatcount = 0;
	private int seatcountloge = 0;
	private Vorstellung vorstellung;
	private ArrayList<String> idList = new ArrayList();

	public Button ctnbutton;
	
	@FXML
	public Label sum;
	
	@FXML
	public Label seatlabel;
	@FXML
	public Label seatlabelloge;
	public MenuItem abusres;
	public AnchorPane content;

	@FXML
	private AnchorPane reservierung;

	@FXML
	private AnchorPane saalplan;

	@FXML
	private Label infos;
	
	@FXML
	private Label kategorie;
	
	@FXML
	private Label kategorie1;
	
	@FXML
	private Label kattext;
	
	@FXML
	private Label kattext1;

	public Main main;

	public void setMain(Main main) {
		this.main = main;
	}

	public AnchorPane getSaalplan() {
		return saalplan;
	}

	public void setVorstellung(String vorstellungId) {
		int id = Integer.parseInt(vorstellungId);
		this.vorstellung = Vorstellung.findVorstellungById(id);
	}

	public void setInfos() {
		infos.setText(vorstellung.getFilm().getName() + " , Saal " + vorstellung.getSaal().getSaalNr() + " , "
				+ vorstellung.getDatum().getTime().toString());
	}

	public void createSaalplan() {
		int reihen = vorstellung.getSaal().getReihenanzahl();
		int sitze = vorstellung.getSaal().getSitzeProReihe();
		this.setInfos();
		saalplan.getChildren().clear();

		// Weißes rechteck
		Rectangle r = new Rectangle();
		r.setArcHeight(5);
		r.setArcWidth(5);
		r.setFill(Paint.valueOf("Transparent"));
		r.setHeight(322);
		r.setWidth(512);
		r.setLayoutX(0);
		r.setLayoutY(0);
		r.setStroke(Paint.valueOf("white"));
		r.setStrokeType(StrokeType.INSIDE);
		r.setStrokeWidth(3);
		saalplan.getChildren().add(r);

		// VBox für die jeweiligen Reihen
		VBox v = new VBox();
		v.setLayoutX(0);
		v.setLayoutY(0);
		v.setPrefHeight(322);
		v.setPrefWidth(512);
		v.setSpacing(10);
		v.setPadding(new Insets(10, 10, 10, 10));

		vorstellung.refreshFreiePlätze();
		for (int i = 0; i < reihen; i++) {
			// HBox für jede Reihe
			HBox h = new HBox();
			h.setPrefWidth(512);
			h.setPrefHeight(322 / reihen);
			h.setSpacing(10);

			for (int j = 0; j < sitze; j++) {
				// Button für jeden Sitz
				Button b = new Button();
				b.setPrefSize(512 / sitze, 322 / reihen);
				b.setId(i + ":" + j);
				if (vorstellung.getVorstellungsPlaetze()[i][j].isBelegt()) {
					b.getStyleClass().add("orangebutton");
				} else {
					if(vorstellung.getVorstellungsPlaetze()[i][j].getPlatz().getKategorie().getName()=="Loge") {
						kattext1.setText(vorstellung.getVorstellungsPlaetze()[i][j].getPlatz().getKategorie().getName());
						kategorie1.setText(Double.toString(vorstellung.getVorstellungsPlaetze()[i][j].getPlatz().getKategorie().getPreis()));
						b.getStyleClass().add("darkgreybutton");
					}else {
						kattext.setText(vorstellung.getVorstellungsPlaetze()[i][j].getPlatz().getKategorie().getName());
						kategorie.setText(Double.toString(vorstellung.getVorstellungsPlaetze()[i][j].getPlatz().getKategorie().getPreis()));
						b.getStyleClass().add("greybutton");
					}
				}
				b.setOnAction(new EventHandler<ActionEvent>() {

					@FXML
					public void handle(ActionEvent event) {
						Button x = (Button) event.getSource();
						if (x.getStyleClass().contains("greybutton")) {
							seatcount++;
							x.getStyleClass().remove("greybutton");
							x.getStyleClass().add("greenbutton");
							idList.add(x.getId());

						} else if (x.getStyleClass().contains("greenbutton")) {
							x.getStyleClass().remove("greenbutton");
							x.getStyleClass().add("greybutton");
							seatcount--;
							idList.remove(x.getId());
						} else if (x.getStyleClass().contains("darkgreybutton")) {
							seatcountloge++;
							x.getStyleClass().remove("darkgreybutton");
							x.getStyleClass().add("logegreenbutton");
							idList.add(x.getId());
						}else if (x.getStyleClass().contains("logegreenbutton")) {
							seatcountloge--;
							x.getStyleClass().remove("logegreenbutton");
							x.getStyleClass().add("darkgreybutton");
							idList.remove(x.getId());
						}
						ctnbutton.setText(Integer.toString(seatcount + seatcountloge));
						seatlabel.setText(Integer.toString(seatcount) + "x Sitzplatz");
						seatlabelloge.setText(Integer.toString(seatcountloge) + "x Sitzplatz");
						sum.setText(Double.toString(seatcount * Double.parseDouble(kategorie.getText()) + seatcountloge * Double.parseDouble(kategorie1.getText())) + "€");
					}
				});
				h.getChildren().add(b);
			}
			Label l = new Label(Integer.toString(i + 1));
			l.setAlignment(Pos.CENTER);
			l.setContentDisplay(ContentDisplay.CENTER);
			l.setPrefSize(512 / sitze, 322 / reihen);
			l.getStyleClass().add("whiteborder");
			l.setTextFill(Paint.valueOf("white"));

			h.getChildren().add(l);

			v.getChildren().add(h);
		}

		saalplan.getChildren().add(v);
	}


	@FXML
	public void openAboutUs(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("AboutUs.fxml"));
		reservierung.getChildren().setAll(pane);

	}

	@FXML
	public void openVorstellungsuebersicht(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
		reservierung.getChildren().setAll(pane);
	}

	@FXML
	public void openCheckout(ActionEvent e) throws IOException {

		if(sum.getText().equals("0.0€")) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Bitte Wählen Sie mindestens einen Sitzplatz aus um fortzufahren.");

			alert.showAndWait();
		}else {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("Checkout.fxml"));

			AnchorPane pane = loader.load();

			reservierung.getChildren().setAll(pane);

			CheckoutController con = loader.getController();
			con.getParkettcounter().setText(seatlabel.getText());
			con.getLogecounter().setText(seatlabelloge.getText());
			con.getCheckoutsum().setText(sum.getText());
			con.getKat().setText(kattext.getText());
			con.getKat1().setText(kattext1.getText());
			con.getKatp().setText(kategorie.getText());
			con.getKatp1().setText(kategorie1.getText());
			con.getInfos().setText(infos.getText());
			con.setIdList(idList);
			con.setVorstellung(vorstellung);
		}
	}

	@FXML
	public void opencontact(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("Contakt.fxml"));
		reservierung.getChildren().setAll(pane);
	}

	@FXML
	public void openimpressum(ActionEvent e) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("Impressum.fxml"));
		reservierung.getChildren().setAll(pane);
	}
}
