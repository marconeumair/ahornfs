package guiController;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Menu;
import javafx.scene.layout.AnchorPane;

public class AboutUsController {
	
	
	
	@FXML
	private AnchorPane aboutUs;
	
	public Main main;
	
	public void setMain (Main main) {
		this.main = main;
	}
	
	@FXML
	public void openAboutUs(ActionEvent e) throws IOException{
		
			AnchorPane pane = FXMLLoader.load(getClass().getResource("AboutUs.fxml"));
	        aboutUs.getChildren().setAll(pane);
	}
	@FXML
	public void openVorstellungsuebersicht(ActionEvent e) throws IOException{
		
			AnchorPane pane = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
	        aboutUs.getChildren().setAll(pane);
	}

	@FXML
	public void opencontact(ActionEvent e) throws IOException{
		
			AnchorPane pane = FXMLLoader.load(getClass().getResource("Contakt.fxml"));
			aboutUs.getChildren().setAll(pane);
	}
	@FXML
	public void openimpressum(ActionEvent e) throws IOException{
		
			AnchorPane pane = FXMLLoader.load(getClass().getResource("Impressum.fxml"));
			aboutUs.getChildren().setAll(pane);
	}}